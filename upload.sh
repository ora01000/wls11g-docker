docker save -o wls11g.tar tg/wls11g:10.3.6
scp ./wls11g.tar root@orarepo.ora01000.com:/root
ssh root@orarepo.ora01000.com 'docker load -i /root/wls11g.tar'
ssh root@orarepo.ora01000.com 'docker tag tg/wls11g:10.3.6 10.0.1.125:5000/tg/wls11g:10.3.6'
ssh root@orarepo.ora01000.com 'docker push 10.0.1.125:5000/tg/wls11g:10.3.6'
