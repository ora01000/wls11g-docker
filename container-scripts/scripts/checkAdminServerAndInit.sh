#!/bin/bash

## check weblogic server is running

while :
do
    state=`java -classpath ${ORACLE_HOME}/wlserver_10.3/server/lib/weblogic.jar weblogic.Admin -url t3://localhost:7001 -username weblogic -password weblogic123 GETSTATE| cut -d\: -f2`
    if [[ "$state" == *RUNNING* ]];
    then
        break
    else
        sleep 5
    fi
done

echo "<-------------------------- WebLogic Server Running !! -------------------------->"
echo "<----------------- DataSource & Application will be deployed -------------------->"

## create datasource & deploy application

exec ${ORACLE_HOME}/wlserver_10.3/common/bin/wlst.sh ${DOMAIN_HOME}/scripts/deploy_app.py
